import React, { Component } from "react";
import "./App.css";

import Calendar from "./components/Calendar";
import Note from "./components/Note";
import moment from "moment";

export default class App extends Component {
  state = {
    note: "",
    selectedDate: moment().format("DD-MM-YYYY"),
    notes: {},
  };

  onNoteInputChange = (note) => {
    this.setState({
      note,
      notes: { ...this.state.notes, [this.state.selectedDate]: note },
    });
    // TO-DO: method takes new note input, changes note state and adds this note for current date inside notes object
  };

  onDayClick = (selectedDate) => {
    const note = this.state.notes[selectedDate];
    this.setState({ selectedDate, note });
    return selectedDate;
  };

  render() {
    const { selectedDate, note } = this.state;
    return (
      <div className="App">
        <Calendar onDayClick={this.onDayClick} {...{ selectedDate }} />
        <Note onNoteInputChange={this.onNoteInputChange} {...{ note }} />
      </div>
    );
  }
}
