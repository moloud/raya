import React, { Component } from "react";

export default class YearNav extends Component {
  state = {
    showYearNav: false,
  };

  showYearEditor = () => this.setState({ showYearNav: true });

  onKeyUpYear = (e) => {
    if (this.isValidYear(e.target.value)) {
      this.props.onYearChange(e.target.value);
      this.setState({
        showYearNav: false,
      });
    }
  };

  isValidYear = (inputYear) => {
    return parseInt(inputYear) >= 0 && parseInt(inputYear) < 2101;
  };

  onYearChange = (e) =>
    this.props.onYearChange(e.target.value) &&
    this.props.onYearChange(e.target.value);

  render() {
    const { dateContext } = this.props;
    return this.state.showYearNav ? (
      <input
        defaultValue={dateContext.format("YYYY")}
        className="editor-year"
        onKeyUp={this.onKeyUpYear}
        onChange={this.onYearChange}
        type="number"
        placeholder={dateContext.format("YYYY")}
      />
    ) : (
      <span className="label-year" onDoubleClick={this.showYearEditor}>
        {dateContext.format("YYYY")}
      </span>
    );
  }
}
