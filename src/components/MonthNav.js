import React, { Component } from "react";
import SelectMonth from "./SelectMonth";
import moment from "moment";

export default class MonthNav extends Component {
  state = {
    showMonthPopup: false,
  };

  months = moment.months();

  onToggleDropdown = (e) =>
    this.setState((preState) => ({ showMonthPopup: !preState.showMonthPopup }));

  month = () => this.props.dateContext.format("MMMM");

  render() {
    const { dateContext, onMonthChange } = this.props;
    return (
      <span className="label-month" onClick={this.onToggleDropdown}>
        {dateContext.format("MMMM")}
        {this.state.showMonthPopup && (
          <SelectMonth data={this.months} {...{ onMonthChange }} />
        )}
      </span>
    );
  }
}
