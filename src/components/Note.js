import React, { Component } from "react";

export default class Note extends Component {
  onNoteInputChange = (e) => this.props.onNoteInputChange(e.target.value);

  render() {
    const { note } = this.props;
    return (
      <div>
        <textarea
          value={note || ""}
          onChange={this.onNoteInputChange}
          placeholder="Enter your note..."
        ></textarea>
      </div>
    );
  }
}
