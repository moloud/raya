import React, { Component } from "react";
import moment from "moment";
import "../styles/calendar.css";

import MonthNav from "./MonthNav";
import YearNav from "./YearNav";

export default class Calendar extends Component {
  state = {
    dateContext: moment(),
  };

  weekdays = moment.weekdays();
  weekdaysShort = moment.weekdaysShort();
  months = moment.months();

  onMonthChange = (month) =>
    this.setState((preState) => ({
      dateContext: preState.dateContext.month(month),
    }));

  onYearChange = (year) =>
    this.setState((preState) => ({
      dateContext: preState.dateContext.year(year),
    }));

  daysInMonth = () => this.state.dateContext.daysInMonth();

  onDayClick = (day) => {
    this.setState((preState) => ({
      dateContext: preState.dateContext.date(day),
    }));
    this.props.onDayClick(this.state.dateContext.format("DD-MM-YYYY"));
    return this.state.dateContext.format("DD-MM-YYYY");
  };

  isCurrentMonthSelected = () => {
    return this.state.dateContext.format("M") == moment().format("M");
  };
  currentDate = () => {
    return this.state.dateContext.get("date");
  };
  currentDay = () => {
    return moment().format("D");
  };

  firstDayOfMonth = () => {
    let dateContext = this.state.dateContext;
    let firstDay = moment(dateContext).startOf("month").format("d");
    return firstDay;
  };

  makeBlankDays = () => {
    let blanks = [];
    for (let i = 0; i < this.firstDayOfMonth(); i++) {
      blanks.push(
        <td key={i * 80} className="emptySlot">
          {""}
        </td>
      );
    }
    return blanks;
  };

  makeExistingDays = () => {
    let daysInMonth = [];
    for (let d = 1; d <= this.daysInMonth(); d++) {
      let className =
        d == this.currentDate() && this.isCurrentMonthSelected()
          ? "day current-day"
          : "day";
      let selectedClass =
        d == this.props.selectedDate.split("-")[0] ? " selected-day " : "";

      daysInMonth.push(
        <td
          key={d}
          onClick={(e) => {
            this.onDayClick(d);
          }}
          className={className + selectedClass}
        >
          <span>{d}</span>
        </td>
      );
    }

    return daysInMonth;
  };

  makeTableRowElements = (totalSlots) => {
    let rows = [];
    let cells = [];

    totalSlots.forEach((row, i) => {
      if (i % 7 !== 0) {
        cells.push(row);
      } else {
        let insertRow = cells.slice();
        rows.push(insertRow);
        cells = [];
        cells.push(row);
      }
      if (i === totalSlots.length - 1) {
        let insertRow = cells.slice();
        rows.push(insertRow);
      }
    });

    return rows.map((d, i) => {
      return <tr key={i}>{d}</tr>;
    });
  };

  render() {
    const { dateContext } = this.state;
    let weekdays = this.weekdaysShort.map((day) => {
      return (
        <td key={day} className="week-day">
          {day}
        </td>
      );
    });

    let blanks = this.makeBlankDays();
    let daysInMonth = this.makeExistingDays();


    var totalSlots = [...blanks, ...daysInMonth];

    let tableRowElements = this.makeTableRowElements(totalSlots);

    return (
      <div className="calendar-container">
        <table className="calendar">
          <thead>
            <tr className="calendar-header">
              <td colSpan="7">
                <MonthNav
                  {...{ dateContext }}
                  onMonthChange={this.onMonthChange}
                />
                <YearNav
                  {...{ dateContext }}
                  onYearChange={this.onYearChange}
                />
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>{weekdays}</tr>
            {tableRowElements}
          </tbody>
        </table>
      </div>
    );
  }
}